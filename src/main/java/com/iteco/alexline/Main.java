package com.iteco.alexline;

import com.iteco.alexline.builder.EmailCreator;
import com.iteco.alexline.builder.IEmailCreator;
import com.iteco.alexline.entity.Email;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        IEmailCreator emailCreator = new EmailCreator();
        Email email = emailCreator
                .addSubject("Третье домашнее задание")
                .addContactFrom("uranus_123@mail.ru")
                .to("iteco@iteco.ru")
                .toAll(Arrays.asList("suhachev.i@iteco.ru", "google@gmail.com"))
                .to("yandex@ya.ru")
                .copyTo("leningrad@tt.ru")
                .copyToAll(Arrays.asList("abracadabra@iteco.ru", "end.of.imagination@iteco.ru"))
                .addContent("Это первая иттерация третьего задания! Надеюсь понравится :) ")
                .build();

        System.out.println(email);
    }
}