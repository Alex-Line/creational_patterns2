package com.iteco.alexline.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.LinkedHashSet;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
public class Email {

    private String subject;

    private String from = "me@iteco.ru";

    private Set<String> contactsTo = new LinkedHashSet<>();

    private Set<String> contactsCopyTo = new LinkedHashSet<>();

    private String content;

    private String signature = "\n С уважением, " + from + ".";

    public Email(String from, String signature) {
        this.from = from;
        this.signature = signature;
    }

    @Override
    public String toString() {
        return '\n' +
                "<--- Электронное письмо --->" + '\n' +
                "Тема: " + subject + '\n' +
                "от: " + from + '\n' +
                "кому: " + contactsTo + '\n' +
                "копии для: " + contactsCopyTo + '\n' +
                "содержание: " + content;
    }
}