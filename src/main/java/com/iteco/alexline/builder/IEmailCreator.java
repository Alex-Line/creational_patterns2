package com.iteco.alexline.builder;

import org.jetbrains.annotations.NotNull;

public interface IEmailCreator {

    EmailCreator.ISubjectCreator addSubject(@NotNull final String subject);

}