package com.iteco.alexline.builder;

import com.iteco.alexline.entity.Email;
import org.jetbrains.annotations.NotNull;

import java.util.List;

public class EmailCreator implements IEmailCreator {

    private Email email;

    public EmailCreator() {
        this.email = new Email();
    }

    public EmailCreator(Email email) {
        this.email = email;
    }

    class SubjectCreator implements ISubjectCreator {

        public IContactFromCreator addContactFrom(@NotNull final String from) {
            email.setFrom(from);
            return new ContactFromCreator();
        }

    }

    private class ContactFromCreator implements IContactFromCreator {

        public IContactToCreator to(@NotNull final String to) {
            email.getContactsTo().add(to);
            return new ContactToCreator();
        }

        public IContactToCreator toAll(@NotNull final List<String> toAll) {
            toAll.forEach(s -> email.getContactsTo().add(s));
            return new ContactToCreator();
        }

    }

    private class ContactToCreator implements IContactToCreator {

        public IContactToCreator to(@NotNull final String to) {
            email.getContactsTo().add(to);
            return this;
        }

        public IContactToCreator toAll(@NotNull final List<String> toAll) {
            toAll.forEach(s -> email.getContactsTo().add(s));
            return this;
        }

        public IContactCopyCreator copyTo(@NotNull final String copyTo) {
            email.getContactsCopyTo().add(copyTo);
            return new ContactCopyCreator();
        }

        public IContactCopyCreator copyToAll(@NotNull final List<String> copyToAll) {
            copyToAll.forEach(s -> email.getContactsCopyTo().add(s));
            return new ContactCopyCreator();
        }

    }

    private class ContactCopyCreator implements IContactCopyCreator {

        public IContactCopyCreator copyTo(@NotNull final String copyTo) {
            email.getContactsCopyTo().add(copyTo);
            return this;
        }

        public IContactCopyCreator copyToAll(@NotNull final List<String> copyToAll) {
            copyToAll.forEach(s -> email.getContactsCopyTo().add(s));
            return this;
        }

        public IContentCreator addContent(@NotNull final String content) {
            email.setContent(content);
            return new ContentCreator();
        }

    }

    private class ContentCreator implements IContentCreator {

        public Email build() {
            email.setSignature("\n С уважением, " + email.getFrom() + ".");
            email.setContent(email.getContent() + "\n" + email.getSignature());
            return email;
        }

    }

    @Override
    public ISubjectCreator addSubject(@NotNull final String subject) {
        email.setSubject(subject);
        return new SubjectCreator();
    }

    public interface IContactToCreator extends IContactFromCreator {

        IContactCopyCreator copyTo(@NotNull final String copyTo);

        IContactCopyCreator copyToAll(@NotNull final List<String> copyToAll);

    }

    public interface IContentCreator {

        public Email build();

    }

    public interface ISubjectCreator {

        IContactFromCreator addContactFrom(@NotNull final String from);

    }

    public interface IContactCopyCreator {

        IContactCopyCreator copyTo(@NotNull final String copyTo);

        IContactCopyCreator copyToAll(@NotNull final List<String> copyToAll);

        EmailCreator.IContentCreator addContent(@NotNull final String content);

    }

    public interface IContactFromCreator {

        EmailCreator.IContactToCreator to(@NotNull final String to);

        EmailCreator.IContactToCreator toAll(@NotNull final List<String> toAll);

    }

}